create table TblOrderHeader
( Id int primary key identity (1,1),
CodeTransaction nvarchar (20) not null,
IdCustomer int not null,
Amount decimal (18,2) not null,
TotalQty int not null,
IsCheckOut bit not null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)


create table TblOrderDetail(
id int primary key identity (1,1),
IdHeader int not null,
IdProduct int not null,
Qty int not null,
SumPrice decimal (18,2) not null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

select * from TblOrderDetail

select * from TblOrderHeader