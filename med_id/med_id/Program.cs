using Microsoft.EntityFrameworkCore;
using med_id.datamodels;
using med_id.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();



//Unable to resolve service for type 'Xpos334.Services.CategoryService' while attempting to activate 'Xpos334.Controllers.CategoryController'.
//eror diatas pake solusi dibawah ini 
//nambahin 
builder.Services.AddScoped<AuthService>();
builder.Services.AddScoped<MRoleService>();
builder.Services.AddScoped<MLocationService>();
builder.Services.AddScoped<MLocatioanLevelService>();
builder.Services.AddScoped<MBiodataService>();


//add session
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddSession(option =>
{
    option.IdleTimeout = TimeSpan.FromHours(1);
    option.Cookie.HttpOnly = true;
    option.Cookie.IsEssential = true;
});



//add connection string
builder.Services.AddDbContext<DB_SpecificationContext>(option =>
{
    option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

//add Session
app.UseSession();



app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
