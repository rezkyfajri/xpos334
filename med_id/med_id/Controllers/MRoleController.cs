﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Drawing.Printing;

namespace med_id.Controllers
{
    public class MRoleController : Controller
    {
        private MRoleService mRoleService;
        VMResponse respon = new VMResponse();

        public MRoleController(MRoleService _mRoleService)
        {
            mRoleService = _mRoleService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<MRole> data = await mRoleService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(PaginatedList<MRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }





        public async Task<IActionResult>Edit_MenuAccess(int id)
        {
            VMMRole data = await mRoleService.GetDataById_MenuAccess(id);
            ViewBag.role_menu = data.role_menu;
            return PartialView(data);
        }



        [HttpPost]
        public async Task<IActionResult> Edit_MenuAccess(VMMRole dataParam)
        {
            dataParam.ModifiedBy = 1;
            VMResponse respon = await mRoleService.Edit_MenuAccess(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }






    }
}
