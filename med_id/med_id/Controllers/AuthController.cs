﻿using Microsoft.AspNetCore.Mvc;using med_id.Services;
using med_id.viewmodels;
using System.Net.Mail;
using System.Net;
using med_id.datamodels;

namespace med_id.Controllers
{
    public class AuthController : Controller
    {
        private AuthService authService;
        private MRoleService mRoleService;
        VMResponse respon = new VMResponse();

        public AuthController(AuthService _authService, MRoleService _mRoleService)
        {
            authService = _authService;
            mRoleService = _mRoleService;

        }


        public IActionResult Register()
        {
            return PartialView();
        }

        public IActionResult Login()
        {
            return PartialView();
        }


        public async Task<IActionResult> CreateOTP(TToken dataParam)
        {
            var OTP = GenerateOTP();
            var fromMail = new MailAddress("rezkyfajri08@gmail.com", "Admin");
            var fromEmailpassword = "smzk dcyd ihgw zopz";
            var toMail = new MailAddress(dataParam.Email);

            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(fromMail.Address, fromEmailpassword);

            var Message = new MailMessage(fromMail, toMail);
            Message.Subject = "Register Account";
            Message.Body = "<br/> OTP : " + OTP;
            Message.IsBodyHtml = true;

            smtp.Send(Message);
            dataParam.UsedFor = "Register";
            dataParam.Token = OTP;
            dataParam.CreatedBy = 1;

            bool cek = await authService.CheckEmailAlready(dataParam.Email);
            if (cek)
            {
                VMResponse responUpdate = await authService.Edit(dataParam);
                VMResponse respon = await authService.CheckOTP(dataParam);

            }
            else
            {
                VMResponse respon = await authService.CheckOTP(dataParam);

            }

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(respon);
            
        }

        public IActionResult ConfirmOTP(string email)
        {
            ViewBag.email = email;
            return PartialView();
        }


        public async Task<JsonResult> SubmitOTP(string email, string token, TToken dataParam)
        {
            VMResponse respon = await authService.CheckIsExpired(dataParam);
            bool data = await authService.CheckTokenByEmail(email, token);
            return Json(data);       
            
        }



        public async Task<JsonResult> CheckEmailIsExist(string email)
        {

            bool isExist = await authService.CheckEmailIsExist(email);
            return Json(isExist);
        }

       

        public async Task<IActionResult> SetPassword(string email)
        {
            ViewBag.email = email;
            return PartialView();

        }

        public string GenerateOTP()
        {
            string OTPLength = "6";
            string OTP = string.Empty;

            string Chars = string.Empty;
            Chars = "1,2,3,4,5,6,7,8,9,0";

            char[] seplitChar = { ',' };
            string[] arr = Chars.Split(seplitChar);
            string NewOTP = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < Convert.ToInt32(OTPLength); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                NewOTP += temp;
                OTP = NewOTP;
            }
            return OTP;
        }


        public async Task<IActionResult> SetBiodata(string password, string email)
        {

            VMMUser data = new VMMUser();

            List<MRole> listRole = await mRoleService.GetAllData();
            ViewBag.ListRole = listRole;


            ViewBag.email = email;
            ViewBag.password = password;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMMUser dataParam)
        {
            dataParam.CreatedBy = 1;
            if(dataParam.Mobile_phone ==null)
            {
                dataParam.Mobile_phone = "";
            }
            if(dataParam.RoleId == null)
            {
                dataParam.RoleId = 0;
            }
            VMResponse respon = await authService.Create(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }





    }
}
