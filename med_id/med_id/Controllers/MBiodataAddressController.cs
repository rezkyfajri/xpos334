﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Reflection.Emit;

namespace med_id.Controllers
{
    public class MBiodataAddressController : Controller
    {

      
        private MBiodataService mBiodataService;
        private MLocationService mLocatioanService;
        VMResponse respon = new VMResponse();

        public MBiodataAddressController(MBiodataService _mBiodataService ,MLocationService _mLocationService)
        {

            mBiodataService = _mBiodataService;
            mLocatioanService = _mLocationService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize, string dropDownLabel, string curentdropDownLabel)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (dropDownLabel != null)
            {
                curentdropDownLabel = dropDownLabel;

            }

            ViewBag.CurentdropDownLabel = dropDownLabel;


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            long IdUser = 1;
            List<VMAlamat> data = await mBiodataService.GetAllData(IdUser);


            List<string> label = new List<string>();

            foreach (var item in data)
            {
                label.Add(item.Label);
            }

            ViewBag.listLabel = label.Distinct();



            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Label.ToLower().Contains(searchString.ToLower()) ||
                a.Address.ToLower().Contains(searchString.ToLower())).ToList();
            }
            

            if (dropDownLabel != null)
            {
                data = data.Where(a => a.Label == dropDownLabel).ToList();
            }



            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Label).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Label).ToList();
                    break;
            }

            return View(PaginatedList<VMAlamat>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }

        public async Task<IActionResult> Create()
        {
            VMAlamat data = new VMAlamat();

            long IdUser = 1;
            List<VMMLocation> ListLocation = await mLocatioanService.GetAllDataKecDanKota();
            ViewBag.ListLocation = ListLocation;
            return PartialView(data);
        }

        


        

        [HttpPost]
        public async Task<IActionResult> Create(VMAlamat dataParam)
        {
            long IdUser = 1;
            VMAlamat data = await mBiodataService.GetBiodataId(IdUser);
            dataParam.BiodataId = data.BiodataId;
            VMResponse respon = await mBiodataService.Create(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        


       public async Task<IActionResult> MultipleDelete(List<int> listId)
        {
            List<string> listName = new List<string>();
            foreach (var item in listId)
            {
                VMAlamat data = await mBiodataService.GetDataById(item);
                listName.Add(data.Address);
            }

            ViewBag.ListName = listName;
            return PartialView();
        }


        [HttpPost]
        public async Task<IActionResult> SureMultipleDelete(List<int> listId)
        {
            VMResponse respon = await mBiodataService.MultipleDelete(listId);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }


        /*public async Task<long> GetBiodataId(long IdUser)
        {
            MBiodataAddress getbiodataId = await mBiodataService.GetBiodataId(IdUser);
            long BiodataId = getbiodataId.BiodataId;
            return BiodataId;
        }*/

        public async Task<IActionResult> Edit(int id)
        {

            VMAlamat data = await mBiodataService.GetDataById(id);
           
            List<VMMLocation> ListLocation = await mLocatioanService.GetAllDataKecDanKota();
            ViewBag.ListLocation = ListLocation;


            return PartialView(data);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(VMAlamat dataParam)
        {
            


            VMResponse respon = await mBiodataService.Edit(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMAlamat data = await mBiodataService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(long id)
        {
            VMResponse respon = await mBiodataService.Delete(id);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }


        public async Task<JsonResult> CheckNameIsExist(string label, long id)
        {
            bool isExist = await mBiodataService.CheckNameIsExist(label, id);
            return Json(isExist);
        }


    }
}
