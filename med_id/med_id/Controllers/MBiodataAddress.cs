﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class MBiodataAddress : Controller
    {

        private AuthService authService;
        private MRoleService mRoleService;
        VMResponse respon = new VMResponse();

        public MBiodataAddress(AuthService _authService, MRoleService _mRoleService)
        {
            authService = _authService;
            mRoleService = _mRoleService;

        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize, long? id)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (id == null)
            {
                id = 0;
            }


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;


            /*List<VMMLocation> data = await mLocationService.GetAllDataLocation();*/





            /*MLocationLevel dataLocation = new MLocationLevel();
            List<MLocationLevel> listLocationLevel = await mLocationLevelService.GetAllData();
            ViewBag.listLocationLevel = listLocationLevel;*/
/*
            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }



            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }*/

            return View(PaginatedList<VMMLocation>.CreateAsync(/*data*/ pageNumber ?? 1, pageSize ?? 5));
        }
    }
}
