﻿using med_id.api.Services;
using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class MLocationController : Controller
    {
        private MLocationService mLocationService;
        private MLocatioanLevelService mLocationLevelService;
        VMResponse respon = new VMResponse();

        public MLocationController(MLocationService _mLocationService, MLocatioanLevelService _mLocationLevelService)
        {
            mLocationService = _mLocationService;
            mLocationLevelService = _mLocationLevelService;
        }


        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize, long? LevelLocation, long? curentIdLocationLevel)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.LocationLevelSort = sortOrder == "LocationLevelId" ? "LocationLevelId_desc" : "LocationLevelId";
            if(LevelLocation == null)
            {
                LevelLocation = curentIdLocationLevel;

            }
            
            ViewBag.CurentIdLocationLevel = LevelLocation;

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;


            List<VMMLocation> data = await mLocationService.GetAllDataLocation();





            MLocationLevel dataLocation = new MLocationLevel();
            List<MLocationLevel> listLocationLevel = await mLocationLevelService.GetAllData();
            ViewBag.listLevelLocation = listLocationLevel;

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }
            if(LevelLocation != null)
            {
                data = data.Where(a=>a.LocationLevelId == LevelLocation).ToList();
            }



            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                case "LocationLevelId_desc":
                    data = data.OrderByDescending(data=> data.LocationLevelId).ToList(); 
                    break;
                case "LocationLevelId":
                    data = data.OrderBy(data => data.LocationLevelId).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(PaginatedList<VMMLocation>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }



        //public async Task<IActionResult> Create()
        //{

        //    VMMLocation data = new VMMLocation();
        //    List<MLocationLevel> listLocationLevel = await mLocationLevelService.GetAllData();
        //    ViewBag.listLocationLevel = listLocationLevel;
        //    return PartialView(data);
        //}
        public async Task<IActionResult> Create()
        {
            VMMLocation data = new VMMLocation();

            List<MLocationLevel> ListLevel = await mLocationLevelService.GetAllData();
            ViewBag.ListLevel = ListLevel;
            List<VMMLocation> ListLocation = await mLocationService.GetAllData();
            ViewBag.ListLocation = ListLocation;
            return PartialView(data);
        }

        public async Task<JsonResult> GetDataByIdLevel(int id)
        {
            List<VMMLocation> data = await mLocationService.GetDataByIdLevel(id);
            return Json(data);

        }

        public async Task<JsonResult> CheckNameIsExist(long locationLevelId, string name, int id, long parentId )
        {
            bool isExist = await mLocationService.CheckNameIsExist(locationLevelId,name,id,parentId);
            return Json(isExist);
        }


        [HttpPost]
        public async Task<IActionResult> Create(VMMLocation dataParam)
        {

            VMResponse respon = await mLocationService.Create(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }


        public async Task<IActionResult> Edit(int id)
        {
            VMMLocation data = await mLocationService.GetDataById(id);

            List<MLocationLevel> ListLevel = await mLocationLevelService.GetAllData();
            ViewBag.ListLevel = ListLevel;
            long? level = data.LocationLevelId; 
            List<VMMLocation> ListLocation = await mLocationService.GetAllDataEdit(level);

            
            ViewBag.ListLocation = ListLocation;




            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMMLocation dataParam)
        {
            VMResponse respon = await mLocationService.Edit(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }


        public async Task<IActionResult> Delete(int id)
        {
            VMMLocation data = await mLocationService.GetDataById(id);
            return PartialView(data);
        }

       
        public async Task<JsonResult> CheckWilayahInUserOrNot(long id)
        {
            bool isExist = await mLocationService.CheckWilayahInUserOrNot(id);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(long id)
        {
            VMResponse respon = await mLocationService.Delete(id);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }

    }
}
