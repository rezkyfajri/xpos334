﻿using med_id.datamodels;
using med_id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace med_id.Services
{
    public class MBiodataService
    {

        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public MBiodataService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMAlamat>> GetAllData(long IdUser)
        {
            List<VMAlamat> data = new List<VMAlamat>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMBiodata/GetAllData/{IdUser}");
            data = JsonConvert.DeserializeObject<List<VMAlamat>>(apiResponse);
            return data;
        }

        public async Task<List<MBiodataAddress>> GetAllDataLabel()
        {
            List<MBiodataAddress> data = new List<MBiodataAddress>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiMBiodata/GetAllDataLabel");
            data = JsonConvert.DeserializeObject<List<MBiodataAddress>>(apiResponse);
            return data;
        }


        public async Task<VMResponse> Create(VMAlamat dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PostAsync(RouteAPI + "apiMBiodata/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }



        public async Task<VMAlamat> GetDataById(int id)
        {
            VMAlamat data = new VMAlamat();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMBiodata/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMAlamat>(apiResponse);
            return data;
        }


        public async Task<VMAlamat> GetBiodataId(long IdUser)
        {
            VMAlamat data = new VMAlamat();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMBiodata/GetDataById/{IdUser}");
            data = JsonConvert.DeserializeObject<VMAlamat>(apiResponse);
            return data;
        }


        public async Task<VMResponse> MultipleDelete(List<int> listId)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(listId);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiMBiodata/MultipleDelete", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


        public async Task<VMResponse> Edit(VMAlamat dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PutAsync(RouteAPI + "apiMBiodata/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(long id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiMBiodata/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


        public async Task<bool> CheckNameIsExist(string label, long id)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMBiodata/CheckNameIsExist{label}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }


    }
}
