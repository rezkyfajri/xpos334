﻿using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using System.Text;

namespace med_id.Services
{
    public class MLocationService
    {

        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public MLocationService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }


        public async Task<List<VMMLocation>> GetAllDataLocation()
        {
            List<VMMLocation> data = new List<VMMLocation>();

            //menjdapatkan api 
            string apiRespon = await client.GetStringAsync(RouteAPI + "apiMLocation/GetAllDataLocation");
            //mengubah string menjadi object list 
            data = JsonConvert.DeserializeObject<List<VMMLocation>>(apiRespon);
            return data;
        }
        public async Task<List<VMMLocation>> GetDataByIdLevel(int id)
        {
            List<VMMLocation> data = new List<VMMLocation>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMLocation/GetDataByIdLevel/{id}");
            data = JsonConvert.DeserializeObject<List<VMMLocation>>(apiResponse);
            return data;
        }

        public async Task<List<VMMLocation>> GetAllData()
        {
            List<VMMLocation> data = new List<VMMLocation>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiMLocation/GetAllDataLocation");
            data = JsonConvert.DeserializeObject<List<VMMLocation>>(apiResponse);
            return data;
        }

        public async Task<List<VMMLocation>> GetAllDataKecDanKota()
        {
            List<VMMLocation> data = new List<VMMLocation>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiMLocation/GetAllDataKecDanKota");
            data = JsonConvert.DeserializeObject<List<VMMLocation>>(apiResponse);
            return data;
        }

        public async Task<List<VMMLocation>> GetAllDataEdit(long? level)
        {
            List<VMMLocation> data = new List<VMMLocation>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMLocation/GetAllDataLocationEdit/{level}");
            data = JsonConvert.DeserializeObject<List<VMMLocation>>(apiResponse);
            return data;
        }


        public async Task<bool> CheckNameIsExist(long locationLevelId, string name , int id, long parentId)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMLocation/CheckNameIsExist/{locationLevelId}/{name}/{id}/{parentId}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }

        public async Task<VMResponse> Create(VMMLocation dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PostAsync(RouteAPI + "apiMLocation/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


        public async Task<VMMLocation> GetDataById(int id)
        {
            VMMLocation data = new VMMLocation();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMlocation/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMMLocation>(apiResponse);
            return data;
        }


        public async Task<VMResponse> Edit(VMMLocation dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PutAsync(RouteAPI + "apiMLocation/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


        public async Task<bool> CheckWilayahInUserOrNot(long id)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMLocation/CheckWilayahInUserOrNot/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }
        public async Task<VMResponse> Delete(long id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiMlocation/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


    }
}
