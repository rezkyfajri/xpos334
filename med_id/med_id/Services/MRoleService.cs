﻿using med_id.datamodels;
using med_id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace med_id.Services
{
    public class MRoleService
    {

        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();




        public MRoleService(IConfiguration _configuration) //cara dapatkn alamat API dari front end JSONSETTING FRONT END
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }


        public async Task<List<MRole>> GetAllData()
        {
            List<MRole> data = new List<MRole>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiMRole/GetAllData");
            data = JsonConvert.DeserializeObject<List<MRole>>(apiResponse);
            return data;
        }


        public async Task<VMMRole> GetDataById_MenuAccess(int id)
        {
            VMMRole data = new VMMRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMRole/GetDataById_MenuAccess/{id}"); //get url API
            data = JsonConvert.DeserializeObject<VMMRole>(apiResponse); //deserialis mengubah json menjadi objek, serialis sebaliknya
            return data;
        }


        public async Task<VMResponse> Edit_MenuAccess(VMMRole dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiMRole/Edit_MenuAccess", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }




    }
}
