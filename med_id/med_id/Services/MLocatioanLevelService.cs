﻿using med_id.datamodels;
using med_id.viewmodels;
using Newtonsoft.Json;

namespace med_id.Services
{
    public class MLocatioanLevelService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public MLocatioanLevelService(IConfiguration _configuration) //cara dapatkn alamat API dari front end JSONSETTING FRONT END
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }


        public async Task<List<MLocationLevel>> GetAllData()
        {
            List<MLocationLevel> data = new List<MLocationLevel>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiMLocationLevel/GetAllData");
            data = JsonConvert.DeserializeObject<List<MLocationLevel>>(apiResponse);
            return data;
        }



    }
}
