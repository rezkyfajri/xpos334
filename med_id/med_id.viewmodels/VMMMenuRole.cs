﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMMMenuRole
    {
        public long Id { get; set; }
        public long? MenuId { get; set; }
        public long? RoleId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }

        public string? Name { get; set; } 
        public long? ParentId { get; set; }
        public bool is_selected { get; set; }
        public List<VMMMenuRole>? ListChild { get; set; }


    }
}
