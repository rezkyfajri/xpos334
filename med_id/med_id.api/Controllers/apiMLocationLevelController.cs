﻿using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace med_id.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiMLocationLevelController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public apiMLocationLevelController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MLocationLevel> GetAllData()
        {
            List<MLocationLevel> data = db.MLocationLevels.Where(a => a.IsDelete == false).ToList();
            return data;
        }
    }
}
