﻿using med_id.api.Services;
using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace med_id.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiMRoleController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;
        private RolesService rolesService;


        public apiMRoleController(DB_SpecificationContext _db)
        {
            db = _db;
            rolesService = new RolesService(db);
        }


        [HttpGet("GetAllData")]
        public List<MRole> GetAllData()
        {

            List<MRole> data = db.MRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }



        [HttpGet("GetDataById_MenuAccess/{id}")]
        public async Task<VMMRole> DataById_MenuAccess(int id)
        {
            //TblRole result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault();
            VMMRole result = db.MRoles.Where(a => a.Id == id)
                                .Select(a => new VMMRole()
                                {
                                    Id = a.Id,
                                    Name = a.Name,
                                    Code = a.Code,
                                    IsDelete = a.IsDelete,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn,

                                }).FirstOrDefault()!;
            result.role_menu = await rolesService.GetMenuAccessParentChildByRoleID(result.Id, 0, false);
            return result;
        }



        [HttpPut("Edit_MenuAccess")]
        public VMResponse Edit_MenuAccess(VMMRole data)
        {
            MRole dt = db.MRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);

                    //SAVE MenuAccess
                    List<MMenuRole> roleMenuDB = db.MMenuRoles.Where(a => a.RoleId == data.Id).ToList();

                    if (roleMenuDB.Count() > 0)
                    {
                        // delete unused data 
                        List<MMenuRole> roleMenuRemove = roleMenuDB.Where(a =>
                        !(data.role_menu.Where(b => b.is_selected && b.MenuId == a.MenuId).Select(b => b.Id)).Any()
                        ).ToList();
                        foreach (MMenuRole item in roleMenuRemove)
                        {
                            //db.Remove(item);

                            item.IsDelete = true;
                            item.ModifiedBy = IdUser;
                            item.ModifiedOn = DateTime.Now;
                            db.Update(item);
                        }

                        // update existing data
                        List<MMenuRole> roleMenuUpdate = roleMenuDB.Where(a =>
                        (data.role_menu.Where(b => b.is_selected && b.MenuId == a.MenuId).Select(b => b.Id)).Any()
                        ).ToList();
                        foreach (MMenuRole item in roleMenuUpdate)
                        {
                            if (item.IsDelete == true)
                            {
                                item.IsDelete = false;
                                item.ModifiedBy = IdUser;
                                item.ModifiedOn = DateTime.Now;
                                db.Update(item);
                            }
                        }
                    }

                    // insert new data role menu
                    List<MMenuRole> roleMenuAdd = data.role_menu.Where(a =>
                    !(roleMenuDB.Where(b => b.MenuId == a.MenuId).Select(b => b.Id)).Any() && a.is_selected
                        ).Select(a => new MMenuRole()
                        {
                            MenuId = a.MenuId,
                            RoleId = data.Id,
                            IsDelete = false,
                            CreatedBy = IdUser,
                            CreatedOn = DateTime.Now
                        }).ToList();
                    foreach (MMenuRole item in roleMenuAdd)
                    {
                        db.Add(item);
                    }

                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }

        //[HttpPut("Edit_MenuAccess")]
        //public VMResponse Edit_MenuAccess(VMMRole data)
        //{
        //    MRole dt = db.MRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

        //    if (dt != null)
        //    {
        //        dt.Name = data.Name;
        //        dt.ModifiedBy = IdUser;
        //        dt.ModifiedOn = DateTime.Now;

        //        try
        //        {
        //            db.Update(dt);

        //            //SAVE MenuAccess
        //            if (data.role_menu.Count() > 0)
        //            {
        //                //Remove MenuAccess
        //                List<MMenuRole> ListMenuAccessRemove = db.MMenuRoles.Where(a => a.RoleId == data.Id).ToList();
        //                if (ListMenuAccessRemove.Count() > 0)
        //                {
        //                    foreach (MMenuRole item in ListMenuAccessRemove)
        //                    {
        //                        item.IsDelete = true;
        //                        item.ModifiedBy = IdUser;
        //                        item.ModifiedOn = DateTime.Now;
        //                        db.Update(item);
        //                    }
        //                }

        //                //Insert MenuAccess
        //                List<MMenuRole> ListMenuAccessAdd = data.role_menu.Where(a => a.is_selected == true)
        //                                                        .Select(a => new MMenuRole()
        //                                                        {
        //                                                            RoleId = data.Id,
        //                                                            MenuId = a.MenuId,
        //                                                            IsDelete = false,
        //                                                            CreatedBy = IdUser,
        //                                                            CreatedOn = DateTime.Now

        //                                                        }).ToList();

        //                foreach (MMenuRole item in ListMenuAccessAdd)
        //                {
        //                    db.Add(item);
        //                }
        //            }

        //            db.SaveChanges();

        //            respon.Message = "Data success saved";
        //        }
        //        catch (Exception e)
        //        {
        //            respon.Success = false;
        //            respon.Message = "Failed saved : " + e.Message;
        //        }
        //    }
        //    else
        //    {
        //        respon.Success = false;
        //        respon.Message = "Data not found";
        //    }

        //    return respon;
        //}


    }
}
