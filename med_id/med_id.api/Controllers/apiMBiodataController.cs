﻿
using med_id.api.Services;
using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection.Emit;

namespace med_id.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiMBiodataController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();


        public apiMBiodataController(DB_SpecificationContext _db)
        {
            db = _db;
        }



        [HttpGet("GetAllData/{IdUser}")]
        public List<VMAlamat> GetAllData(long IdUser)
        {
            List<VMAlamat> data = (from ba in db.MBiodataAddresses
                                   join l in db.MLocations on ba.LocationId equals l.Id
                                   join b in db.MBiodata on ba.BiodataId equals b.Id
                                   join u in db.MUsers on b.Id equals u.BiodataId
                                   where ba.IsDelete == false && u.Id == IdUser 
                                   select new VMAlamat
                                   {    
                                       Id = ba.Id,
                                       IdUser = IdUser,
                                       BiodataId = ba.BiodataId,
                                       FullName = b.Fullname,
                                       Label = ba.Label,
                                       Recipient = ba.Recipient,
                                       LocationId = ba.LocationId,
                                       RecipientPhoneNumber = ba.RecipientPhoneNumber,
                                       NameLocation = l.Name,
                                       PostalCode = ba.PostalCode,
                                       Address = ba.Address,
                                       IsDelete = ba.IsDelete,
                                       CreatedOn = ba.CreatedOn,

                                   }).ToList();
            return data;
        }




        [HttpPost("Save")]
        public VMResponse Save(MBiodataAddress data)
        {

           
       
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpGet("GetDataById/{id}")]
        public VMAlamat GetDataById(int id)
        {
            VMAlamat data = (from ba in db.MBiodataAddresses
                                join b in db.MBiodata on ba.BiodataId equals b.Id
                                join l in db.MLocations on ba.LocationId equals l.Id
                                where ba.IsDelete == false && ba.Id == id
                                select new VMAlamat
                                {
                                    Id = ba.Id,
                                    BiodataId = ba.BiodataId,
                                    FullName = b.Fullname,
                                    Label = ba.Label,
                                    Recipient = ba.Recipient,
                                    LocationId = ba.LocationId,
                                    RecipientPhoneNumber = ba.RecipientPhoneNumber,
                                    NameLocation = l.Name,
                                    PostalCode = ba.PostalCode,
                                    Address = ba.Address,
                                    IsDelete = ba.IsDelete,
                                    CreatedOn = ba.CreatedOn,
                                }).FirstOrDefault()!;
            return data;
        }


        [HttpGet("GetBiodataId/{IdUser}")]
        public VMAlamat GetBiodataId(long? IdUser)
        {
            VMAlamat data = (from u in db.MUsers
                             join b in db.MBiodata on u.BiodataId equals b.Id
                             where u.Id == IdUser && u.IsDelete == false && b.IsDelete == false
                             select new VMAlamat
                             {
                                 BiodataId = b.Id,
                             }).FirstOrDefault()!;
            return data;
        }

        [HttpPut("MultipleDelete")]
        public VMResponse MultipleDelete(List<int> listId)
        {
            if (listId.Count > 0)
            {
                foreach (int item in listId)
                {
                    MBiodataAddress dt = db.MBiodataAddresses.Where(a => a.Id == item).FirstOrDefault()!;
                    dt.IsDelete = true;
                    dt.ModifiedBy = 1;
                    dt.ModifiedOn = DateTime.Now;
                    db.Update(dt);
                }

                try
                {
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }


        [HttpPut("Edit")]
        public VMResponse Edit(VMAlamat data)
        {
            MBiodataAddress dt = db.MBiodataAddresses.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Label = data.Label;
                dt.Recipient = data.Recipient;
                dt.RecipientPhoneNumber = data.RecipientPhoneNumber;
                dt.LocationId = data.LocationId;
                dt.PostalCode = data.PostalCode ?? null;
                dt.Address =data.Address;
                dt.ModifiedBy = 1;
                dt.ModifiedOn = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success Saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }


        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(long id)
        {
            MBiodataAddress dt = db.MBiodataAddresses.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = 1;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success Deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }




        [HttpGet("CheckNameIsExist{label}/{id}")]
        public bool CheckNameIsExist(string label, long id)
        {
            MBiodataAddress data = new MBiodataAddress();

            if (id == 0)//create
            {

                data = db.MBiodataAddresses.Where(a => a.Label == label && a.IsDelete == false).FirstOrDefault()!;
            }
            else//edit
            {
                data = db.MBiodataAddresses.Where(a => a.Label == label && a.IsDelete == false && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            return false;
        }



    }
}
