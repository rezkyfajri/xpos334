﻿using med_id.api.Services;
using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace med_id.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiMLocationController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        MLocationApiServices mLocationApiServices;
        private VMResponse respon = new VMResponse();

        public apiMLocationController(DB_SpecificationContext _db)
        {
            db = _db;
            mLocationApiServices = new MLocationApiServices(db);
        }

        [HttpGet("GetAllDataLocation")]
        public async Task<List<VMMLocation>> GetAllDataLocation()
        {
            List<VMMLocation> data = (from L in db.MLocations
                                       join LM in db.MLocationLevels on L.LocationLevelId equals LM.Id
                                       where L.IsDelete == false && LM.IsDelete == false
                                       select new VMMLocation
                                       {

                                           Id = L.Id,
                                           Name = L.Name,
                                           ParentChildName = LM.Abbreviation + " " + L.Name,
                                           Abbreviation = LM.Abbreviation,
                                           ParentId = L.ParentId,
                                           LocationLevelId =  L.LocationLevelId,
                                           LocationLevelName = LM.Name,
                                       }).ToList();
            foreach (VMMLocation item in data) 
            {
                if(item.ParentId != null)
                {
                item.ParentData = await mLocationApiServices.GetParentData(item.ParentId);
                 item.ParentChildName = item.ParentChildName + ", " + item.ParentData.ParentChildName;
                }
            }

            return data;
        }

        [HttpGet("GetAllDataKecDanKota")]
        public async Task<List<VMMLocation>> GetAllDataKecDanKota()
        {
            List<VMMLocation> data = (from L in db.MLocations
                                      join LM in db.MLocationLevels on L.LocationLevelId equals LM.Id
                                      where L.IsDelete == false && LM.IsDelete == false && L.LocationLevelId >= 3
                                      select new VMMLocation
                                      {

                                          Id = L.Id,
                                          Name = L.Name,
                                          ParentChildName = L.Name,
                                          ParentId = L.ParentId,
                                          LocationLevelId = L.LocationLevelId,
                                          LocationLevelName = LM.Name,
                                      }).ToList();
            foreach (VMMLocation item in data)
            {
                if (item.ParentId != null)
                {
                    item.ParentData = await mLocationApiServices.GetParentData(item.ParentId);
                    item.ParentChildName = item.ParentChildName + ", " + item.ParentData.ParentChildName;
                }
            }

            return data;
        }


        [HttpGet("GetDataByIdLevel/{id}")]
        public async Task<List<VMMLocation>> GetDataByIdLevel(int id)
        {
            List<VMMLocation> data = (from L in db.MLocations
                                      join LM in db.MLocationLevels on L.LocationLevelId equals LM.Id
                                      where L.IsDelete == false && LM.IsDelete == false && L.LocationLevelId ==id -1
                                      select new VMMLocation
                                      {
                                          Id = L.Id,
                                          Name = L.Name,
                                          ParentChildName = L.Name,
                                          ParentId = L.ParentId,
                                          LocationLevelId = L.LocationLevelId,
                                          LocationLevelName = LM.Name,
                                      }).ToList();

            foreach (VMMLocation item in data)
            {
                if (item.ParentId != null)
                {

                    item.ParentData = await mLocationApiServices.GetParentData(item.ParentId);
                    item.ParentChildName = item.ParentChildName;
                    if(item.ParentData != null)
                    {
                        item.ParentChildName += ", " + item.ParentData.Name;
                    }


                }
            }
            

            return data;
        }



        [HttpGet("CheckNameIsExist/{locationLevelId}/{name}/{id}/{parentId}")]
        public bool CheckNameIsExist(long locationLevelId, string name, int id)
        {
            MLocation data = new MLocation();

            if (id == 0)//create
            {
                
                data = db.MLocations.Where(a => a.Name == name && a.IsDelete == false && a.LocationLevelId == locationLevelId).FirstOrDefault()!;
            }
            else//edit
            {
                data = db.MLocations.Where(a => a.Name == name && a.IsDelete == false && a.LocationLevelId == locationLevelId && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            return false;
        }


        [HttpPost("Save")]
        public VMResponse Save(MLocation data)
        {
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;
            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }



        [HttpGet("GetDataById/{id}")]
        public async Task<VMMLocation> GetDataById(int id)
        {
            VMMLocation data = (from L in db.MLocations
                                join LM in db.MLocationLevels on L.LocationLevelId equals LM.Id
                                where L.IsDelete == false && LM.IsDelete == false && L.Id == id
                                select new VMMLocation
                                {
                                    Id = L.Id,
                                    Name = L.Name,
                                    ParentId = L.ParentId,
                                    LocationLevelId = L.LocationLevelId,
                                    LocationLevelName = LM.Name,
                                }).FirstOrDefault()!;


            if (data.ParentId != null)
            {

                data.ParentData = await mLocationApiServices.GetParentData(data.ParentId);
                if (data.ParentData != null)
                {
                    data.ParentChildName = data.ParentData.Name;
                    if(data.ParentData.ParentData != null)
                    {
                        data.ParentChildName += ", "+data.ParentData.ParentData.Name;

                    }
                }
                else
                {
                    data.ParentChildName = "-";
                }


            }
            

            return data;
        }



        [HttpGet("GetAllDataLocationEdit/{level}")]
        public async Task<List<VMMLocation>> GetAllDataLocationEdit(long? level)
        {
            List<VMMLocation> data = (from L in db.MLocations
                                      join LM in db.MLocationLevels on L.LocationLevelId equals LM.Id
                                      where L.IsDelete == false && LM.IsDelete == false && L.LocationLevelId == level
                                      select new VMMLocation
                                      {

                                          Id = L.Id,
                                          Name = L.Name,
                                          ParentId = L.ParentId,
                                          LocationLevelId = L.LocationLevelId,
                                          LocationLevelName = LM.Name,
                                      }).ToList();
            foreach (VMMLocation item in data)
            {
                if (item.ParentId != null)
                {
                    item.ParentData = await mLocationApiServices.GetParentData(item.ParentId);
                    item.ParentChildName =  item.ParentData.ParentChildName;
                    if(item.ParentData.ParentData != null) 
                    {
                        item.ParentChildName += ", " + item.ParentData.ParentData.ParentChildName;

                    }
                }
            }

            return data;
        }




        [HttpPut("Edit")]
        public VMResponse Edit(MLocation data)
        {
            MLocation dt = db.MLocations.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.LocationLevelId = data.LocationLevelId;
                dt.ParentId = data.ParentId;
                
                dt.ModifiedBy = 1;
                dt.ModifiedOn = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }



        [HttpGet("CheckWilayahInUserOrNot/{id}")]
        public bool CheckWilayahInUserOrNot(long id)
        {
            MLocation data = new MLocation();

                data = db.MLocations.Where(a => a.ParentId == id && a.IsDelete == false).FirstOrDefault()!;
            
            

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            return false;
        }




        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(long id)
        {
            MLocation dt = db.MLocations.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.DeletedBy = 1;
                dt.DeletedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }



    }
}
