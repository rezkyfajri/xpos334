﻿using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using NuGet.Common;
using System.Net.Mail;
using System.Net;
using NuGet.Protocol;
using System.Linq;
using System.Data.SqlTypes;
using System;
using med_id.api.Services;

namespace med_id.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiAuthController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private RolesService rolesService;


        public apiAuthController(DB_SpecificationContext _db)
        {
            db = _db;
            rolesService = new RolesService(db);
        }

    [HttpGet("CheckEmailIsExist/{email}")]
        public bool CheckEmailIsExist(string email)
        {
            MUser data = new MUser();

          
                data = db.MUsers.Where(a => a.Email == email && a.IsDelete == false).FirstOrDefault()!;
            

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            else
            {
                return false;

            }
        }

         [HttpGet("CheckEmailAlready/{email}")]
        public bool CheckEmailAlready(string email)
        {
            TToken data = new TToken();

          
                data = db.TTokens.Where(a => a.Email == email && a.IsDelete == false && a.IsExpired == false).FirstOrDefault()!;
            

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            else
            {
                return false;


            }
        }

        [HttpGet("CheckTokenByEmail/{email}/{token}")]
        public bool CheckTokenByEmail(string email, string token)
        {
            TToken data = new TToken();
            

            data = db.TTokens.Where(a => a.Email == email && a.Token == token && a.IsDelete == false && a.IsExpired == false ).FirstOrDefault()!;
            
            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        [HttpPost("CheckOTP")]
        public VMResponse CheckOTP(TToken dataParam)
        {

            TToken data = new TToken();

            data.UsedFor = dataParam.UsedFor ?? "";
                data.CreatedBy = 1;
                data.CreatedOn = DateTime.Now;
                data.IsDelete = false;
                data.ExpiredOn = DateTime.Now.AddMinutes(10);
                data.IsExpired = false;
                data.Token = dataParam.Token;
                data.Email = dataParam.Email;
                try
                {
                    db.Add(data);
                    db.SaveChanges();
                    respon.Message = "Data Succes save :v";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed to save Data" + ex.Message;
                }
            
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TToken dataParam)
        {
            TToken data = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false && a.IsExpired == false).FirstOrDefault();

            if (data != null)
            {
                data.IsDelete = true;
                data.IsExpired = true;
                try
                {
                    db.Update(data);
                    db.SaveChanges();
                    respon.Message = "Succes saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed to saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not found";
            }
            return respon;
        }


        [HttpPut("CheckIsExpired")]
        public VMResponse CheckIsExpired(TToken dataParam)
        {
            TToken data = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false).FirstOrDefault();
            DateTime today = DateTime.Now;
            string expired = data.ExpiredOn.ToString();
            DateTime ExpiredOn = DateTime.Parse(expired);
            TimeSpan timeSpan = ExpiredOn - today;  
             

            if (timeSpan.Minutes <= 0)
            {
                data.IsExpired = true;
                try
                {
                    db.Update(data);
                    db.SaveChanges();
                    respon.Message = "Data Expired";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not found";
            }
            return respon;
        }
            



        [HttpPost("Save")]
        public async Task<VMResponse> Save(VMMUser data)
        {
            MBiodatum head = new MBiodatum();
            head.Fullname = data.FullName;
            head.MobilePhone = data.Mobile_phone;
            head.CreatedBy = data.CreatedBy;
            head.CreatedOn = DateTime.Now;
            head.IsDelete = false;
            try
            {
                db.Add(head);
                db.SaveChanges();
                MUser user = new MUser();
                user.BiodataId = head.Id;
                user.RoleId = data.RoleId;
                user.Email = data.Email;
                user.Password = data.Password;
                user.CreatedBy = data.CreatedBy;
                user.CreatedOn = DateTime.Now;
                user.IsDelete = false;
                try
                {
                    db.Add(user);
                    db.SaveChanges();
                    if(data.RoleId == 1)
                    {
                        MAdmin addRole = new MAdmin();
                        addRole.BiodataId = head.Id;
                        addRole.Code = await rolesService.GetDataById(data.RoleId);
                        addRole.CreatedOn = DateTime.Now;
                        addRole.CreatedBy = data.CreatedBy;
                        addRole.IsDelete  = false;
                        try
                        {
                            db.Add(addRole);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            respon.Success = false;
                            respon.Message = "Failed To Saved" + ex.Message;
                        }

                    }
                    else if(data.RoleId == 2)
                    {
                        MDoctor addRole = new MDoctor();
                        addRole.BiodataId = head.Id;
                        addRole.Str = await rolesService.GetDataById(data.RoleId);
                        addRole.CreatedOn = DateTime.Now;
                        addRole.CreatedBy = data.CreatedBy;
                        addRole.IsDelete = false;
                        try
                        {
                            db.Add(addRole);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            respon.Success = false;
                            respon.Message = "Failed To Saved" + ex.Message;
                        }
                    }
                    else if(data.RoleId == 3)
                    {
                        MCustomer addRole = new MCustomer();
                        addRole.BiodataId = head.Id;
                        addRole.CreatedOn = DateTime.Now;
                        addRole.CreatedBy = data.CreatedBy;
                        addRole.IsDelete = false;
                        try
                        {
                            db.Add(addRole);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            respon.Success = false;
                            respon.Message = "Failed To Saved" + ex.Message;
                        }
                    }
                    try
                    {
                        
                        TToken token = db.TTokens.Where(a => a.Email == user.Email && a.IsExpired == false && a.IsDelete == false).FirstOrDefault();
                        token.IsExpired = true;
                        token.UserId = user.Id;
                        try
                        {
                            db.Update(token);
                            db.SaveChanges();
                            respon.Message = "Data Success saved";

                        }
                        catch (Exception ex)
                        {
                            respon.Success = false;
                            respon.Message = "Failed To Saved" + ex.Message;
                        }
                    }
                    catch(Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "Failed To Saved" + ex.Message;
                    }
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed To Saved" + ex.Message;
                }

            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }


    }
}
   