﻿using med_id.datamodels;
using med_id.viewmodels;

namespace med_id.api.Services
{
    public class RolesService
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public RolesService(DB_SpecificationContext _db)
        {
            this.db = _db;
        }

        public async Task<List<VMMMenuRole>> GetMenuAccessParentChildByRoleID(long RoleId, long MenuParent, bool OnlySelected = false)
        {
            List<VMMMenuRole> result = new List<VMMMenuRole>();
            List<MMenu> data = new List<MMenu>();
            if(MenuParent == 0)
            {
                data = db.MMenus.Where(a =>a.ParentId ==null && a.IsDelete == false).ToList();
            }
            else
            {
                data = db.MMenus.Where(a => a.ParentId == MenuParent && a.IsDelete == false).ToList();
            }
            foreach (MMenu item in data)
            {
                VMMMenuRole list = new VMMMenuRole();
                list.MenuId = item.Id;
                list.Name = item.Name;
                list.ParentId = item.ParentId ?? null;
                list.is_selected = db.MMenuRoles.Where(a => a.RoleId == RoleId && a.MenuId == item.Id && a.IsDelete == false).Any();
                list.ListChild = await GetMenuAccessParentChildByRoleID(RoleId, item.Id, OnlySelected);
                if (OnlySelected)
                {
                    if (list.is_selected)
                        result.Add(list);
                }
                else
                {
                    result.Add(list);
                }
            }
            return result;
        }


        public async Task<string> GetDataById(long? id)
        {
            MRole data = db.MRoles.Where(a=>a.Id == id).FirstOrDefault();
            return data.Code;
        }
    }
}
