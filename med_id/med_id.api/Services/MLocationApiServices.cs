﻿using med_id.datamodels;
using med_id.viewmodels;

namespace med_id.api.Services
{
    public class MLocationApiServices
    {

        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public MLocationApiServices(DB_SpecificationContext _db)
        {
            this.db = _db;
        }


        public async Task<VMMLocation> GetParentData(long? id)
        {
            VMMLocation data = new VMMLocation();
            data = (from L2 in db.MLocations
                              join LM2 in db.MLocationLevels on L2.LocationLevelId equals LM2.Id
                              where L2.Id == id && L2.IsDelete == false
                              select new VMMLocation
                              {
                                  Id = L2.Id,
                                  Name = L2.Name,
                                  ParentChildName = LM2.Abbreviation + " " + L2.Name,
                                  Abbreviation = LM2.Abbreviation,
                                  ParentId = L2.ParentId ?? null,
                                  LocationLevelId = L2.LocationLevelId,
                                  LocationLevelName = LM2.Name,
                              }).FirstOrDefault()!;
            if(data.ParentId != null)
            {
                data.ParentData = await GetParentData(data.ParentId);

                return data;
            }
            else
            {

            return data;
            }
           
        }


       

    }
}
