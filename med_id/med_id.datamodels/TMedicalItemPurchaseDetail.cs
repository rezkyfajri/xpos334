﻿using System;
using System.Collections.Generic;

namespace med_id.datamodels
{
    public partial class TMedicalItemPurchaseDetail
    {
        public long Id { get; set; }
        public long? MedicalItemPurchaseId { get; set; }
        public long? MedicalItemId { get; set; }
        public int? Qty { get; set; }
        public long? MedicalFacilityId { get; set; }
        public long? CourirId { get; set; }
        public decimal? SubTotal { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
