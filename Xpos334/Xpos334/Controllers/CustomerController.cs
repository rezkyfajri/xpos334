﻿using Microsoft.AspNetCore.Mvc;
using Xpos334.Controllers;
using Xpos334.Services;
using xpos334.viewmodels;
using xpos334.datamodels;

namespace Xpos334.Controllers
{
    public class CustomerController : Controller
    {
        private CustomerService customerService;
        private RoleService roleService;

        public CustomerController(RoleService _roleService, CustomerService _customerService)
        {
            roleService = _roleService;
            this.customerService = _customerService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<VMTblCustomer> data = await customerService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCustomer.ToLower().Contains(searchString.ToLower())
                    || a.RoleName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }


            return View(PaginatedList<VMTblCustomer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }


        public async Task<IActionResult> Create()
        {
            VMTblCustomer data = new VMTblCustomer();

            List<TblRole> listRole = await roleService.GetAllData();
            ViewBag.ListRole = listRole;

            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Create(VMTblCustomer dataParam)
        {
            dataParam.CreateBy = 0;
            VMResponse respon = await customerService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string email, int id, int idRole)
        {
            bool isExist = await customerService.CheckByName(email, id, idRole);
            return Json(isExist);
        }


        public async Task<IActionResult> Edit(int id)
        {

            VMTblCustomer data = await customerService.GetDataById(id);

            List<TblRole> listRole = await roleService.GetAllData();
            ViewBag.ListRole = listRole;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMTblCustomer dataParam)
        {
            VMResponse respon = await customerService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }



        public async Task<IActionResult> Detail(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }



        public async Task<IActionResult> Delete(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await customerService.Delete(id);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }




    }
}

