﻿using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;
using Xpos334.Services;

namespace Xpos334.Controllers
{
    public class AuthController : Controller
    {

        private AuthService authService;
        private RoleService roleService;
        VMResponse respon = new VMResponse();



        public AuthController(AuthService _authService, RoleService _roleService)
        {
            authService = _authService;
            roleService = _roleService;
        }


        public IActionResult Login()
        {
            return PartialView();
        
        }




        [HttpPost]
        public async Task<IActionResult> LoginSubmit(string email, string password)
        {
            VMTblCustomer customer =await authService.CheckLogin(email,password);
            if(customer != null) 
            {
                respon.message = $"Hello, {customer.NameCustomer} Welcome TO THE JUNGLE";
                HttpContext.Session.SetString("NameCustomer", customer.NameCustomer);
                HttpContext.Session.SetInt32("IdCustomer", customer.Id);
                HttpContext.Session.SetInt32("IdRole", customer.IdRole ?? 0);
            }
            else
            {
                respon.Success = false;
                respon.message = $"Email Or Password not valid";

            }
            return Json(new {dataRespon = respon});
        }



        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
        public async Task<IActionResult> Register()
        {
            VMTblCustomer data = new VMTblCustomer();

            List<TblRole> listRole = await roleService.GetAllData();
            ViewBag.ListRole = listRole;

            return PartialView(data);
        }



    }

}
