﻿using Newtonsoft.Json;
using xpos334.viewmodels;

namespace Xpos334.Services
{
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public AuthService(IConfiguration _configuration) //cara dapatkn alamat API dari front end JSONSETTING FRONT END
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }


        public async Task<VMTblCustomer> CheckLogin(string email, string password)
        {
            VMTblCustomer data = new VMTblCustomer();
            string apiRespon = await client.GetStringAsync(RouteAPI+ $"apiAuth/CheckLogin/{email}/{password}") ;
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiRespon);

                return data;
        }

        public async Task<List<VMMenuAccess>> MenuAccess(int IdRole)
        {
            List<VMMenuAccess> data = new List<VMMenuAccess>();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiAuth/MenuAccess/{IdRole}");
            data = JsonConvert.DeserializeObject<List<VMMenuAccess>>(apiRespon);
            return data;
        }




    }
}
