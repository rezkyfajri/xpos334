﻿using MessagePack.Formatters;
using Newtonsoft.Json;
using System.Text;
using xpos334.viewmodels;

namespace Xpos334.Services
{
    public class OrderService
    {

        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public OrderService(IConfiguration _configuration) //cara dapatkn alamat API dari front end JSONSETTING FRONT END
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<VMResponse> SubmitOrder(VMOrderHeader dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PostAsync(RouteAPI + "apiOrder/SubmitOrder", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<List<VMOrderHeader>> GetDataOrderHeaderDetail(int IdCustomer)
        {
            //   List<VMOrderHeader> data = new List<VMOrderHeader> ();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/GetDataOrderHeaderDetail/{IdCustomer}");
            List<VMOrderHeader> data = JsonConvert.DeserializeObject<List<VMOrderHeader>>(apiResponse);
            return data;
        }

        public async Task<int> CountTransaction(int IdCustomer)
        {

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/CountTransaction/{IdCustomer}");
            int data = JsonConvert.DeserializeObject<int>(apiResponse);
            return data;
        }



    }
}
