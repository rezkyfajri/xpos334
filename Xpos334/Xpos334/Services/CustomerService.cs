﻿using Newtonsoft.Json;
using System.Text;
using xpos334.viewmodels;

namespace Xpos334.Services
{
    public class CustomerService
    {

        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public CustomerService(IConfiguration _configuration) //cara dapatkn alamat API dari front end JSONSETTING FRONT END
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }


        public async Task<List<VMTblCustomer>> GetAllData()
        {
            List<VMTblCustomer> data = new List<VMTblCustomer>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiResponse);
            return data;
        }


        public async Task<VMResponse> Create(VMTblCustomer dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<bool> CheckByName(string email, int id, int idRole)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiCustomer/CheckByName/{email}/{id}/{idRole}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);
            return isExist;
        }



        public async Task<VMTblCustomer> GetDataById(int id)
        {
            VMTblCustomer data = new VMTblCustomer();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(VMTblCustomer dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PutAsync(RouteAPI + "apiCustomer/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCustomer/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }





    }
}
