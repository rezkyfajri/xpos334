﻿    using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos334.api.Services;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiRoleController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private RolesService rolesService;
        private int IdUser = 1;

        public apiRoleController(XPOS_334Context _db)
        {
            db = _db;
            rolesService = new RolesService(db);
        }
        [HttpGet("GetAllData")]
        public List<TblRole> GetAllData()
        {
            List<TblRole> data = db.TblRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }


        [HttpGet("CheckCategoryByName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            TblRole data = new TblRole();
            if (id == 0) //saat create di Front end
            {
                data = db.TblRoles.Where(a => a.RoleName == name && a.IsDelete == false).FirstOrDefault();
            }
            else // saat edit di front end
            {
                data = db.TblRoles.Where(a => a.RoleName == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }
            if (data != null)
            {
                return true;
            }
            return false;
        }


        [HttpPost("Save")]
        public VMResponse Save(TblRole data)
        {
            data.CreatedBy = IdUser;
            data.CreatedDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();
                respon.message = "Data Succes save :v";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.message = "Failed to save Data" + ex.Message;
            }
            return respon;
        }



        [HttpGet("GetDataById/{id}")]
        public TblRole DataById(int id)
        {
            TblRole result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }




        [HttpGet("GetDataById_MenuAccess/{id}")]

        public async Task<VMTblRole> DataById_MenuAccess(int id)
        {
            //TblRole result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault();
            VMTblRole result = db.TblRoles.Where(a => a.Id == id)
                                .Select(a => new VMTblRole()
                                {
                                    Id = a.Id,
                                    RoleName = a.RoleName,
                                }).FirstOrDefault()!;
            result.role_menu = await rolesService.GetMenuAccessParentChildByRoleID(result.Id, 0, false);
            return result;
        }





        [HttpPut("Edit")]
        public VMResponse Edit(TblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault();

            if (dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdatedDate = DateTime.Now;
                dt.UpdatedBy = IdUser;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.message = "Succes saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.message = "Failed to saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.message = "Data Not found";
            }
            return respon;
        }


        [HttpPut("Edit_MenuAccess")]
        public VMResponse Edit_MenuAccess(VMTblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);

                    //SAVE MenuAccess
                    if (data.role_menu.Count() > 0)
                    {
                        //Remove MenuAccess
                        List<TblMenuAccess> ListMenuAccessRemove = db.TblMenuAccesses.Where(a => a.IdRole == data.Id).ToList();
                        if (ListMenuAccessRemove.Count() > 0)
                        {
                            foreach (TblMenuAccess item in ListMenuAccessRemove)
                            {
                                item.IsDelete = true;
                                item.UpdatedBy = IdUser;
                                item.UpdatedDate = DateTime.Now;
                                db.Update(item);
                            }
                        }

                        //Insert MenuAccess
                        List<TblMenuAccess> ListMenuAccessAdd = data.role_menu.Where(a => a.is_selected == true)
                                                                .Select(a => new TblMenuAccess()
                                                                {
                                                                    IdRole = data.Id,
                                                                    MenuId = a.IdMenu,
                                                                    IsDelete = false,
                                                                    CreatedBy = IdUser,
                                                                    CreatedDate = DateTime.Now

                                                                }).ToList();

                        foreach (TblMenuAccess item in ListMenuAccessAdd)
                        {
                            db.Add(item);
                        }
                    }

                    db.SaveChanges();

                    respon.message = "Data success saved";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.message = "Failed saved : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.message = "Data not found";
            }

            return respon;
        }







        [HttpDelete("Delete/{id}/{createBy}")]
        public VMResponse Delete(int id, int createBy)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == id).FirstOrDefault();

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdatedDate = DateTime.Now;
                dt.UpdatedBy = createBy;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.message = "Succes Delete";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.message = "Failed to Delete" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.message = "Data Not found";
            }
            return respon;
        }

    }
}
