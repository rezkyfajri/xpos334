﻿    using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCategoryController(XPOS_334Context _db)
        {
            db = _db;
        }
        [HttpGet("GetAllData")]
        public List<TblCategory> GetAllData() 
        {
            List<TblCategory> data = db.TblCategories.Where(a => a.IsDelete==false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblCategory DataById(int id)
        {
            TblCategory result = db.TblCategories.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }
        [HttpGet("CheckCategoryByName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            TblCategory data = new TblCategory();
            if(id ==0) //saat create di Front end
            {
                data = db.TblCategories.Where(a => a.NameCategory == name && a.IsDelete == false).FirstOrDefault();
            }
            else // saat edit di front end
            {
                data = db.TblCategories.Where(a => a.NameCategory == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }
            if(data != null)
            {
                return true;
            }           
                return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCategory data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;
            data.Description = data.Description ?? "";

            try
            {
                db.Add(data);
                db.SaveChanges();
                respon.message = "Data Succes save :v";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.message = "Failed to save Data" + ex.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCategory data) 
        {
            TblCategory dt = db.TblCategories.Where(a => a.Id == data.Id).FirstOrDefault();

            if(dt != null)
            {
                dt.NameCategory = data.NameCategory;
                dt.Description = data.Description ?? "";
                dt.UpdateDate = DateTime.Now;
                dt.UpdateBy = IdUser;
                try
                {
                    db.Update(dt);
                    db.SaveChanges() ;
                    respon.message = "Succes saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.message = "Failed to saved"+ex.Message;
                }
              
            }
            else
            {
                respon.Success = false;
                respon.message = "Data Not found";
            }
            return respon;  
        }

        [HttpDelete("Delete/{id}/{createBy}")]
        public VMResponse Delete(int id, int createBy)
        {
            TblCategory dt = db.TblCategories.Where(a => a.Id == id ).FirstOrDefault();

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateDate = DateTime.Now;
                dt.UpdateBy = createBy;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.message = "Succes Delete";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.message = "Failed to Delete" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.message = "Data Not found";
            }
            return respon;
        }



    }
}
