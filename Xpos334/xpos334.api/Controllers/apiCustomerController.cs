﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using xpos334.datamodels;
using xpos334.viewmodels;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {

        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCustomerController(XPOS_334Context _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = (from c in db.TblCustomers
                                       join r in db.TblRoles on   c.IdRole equals r.Id into tc
                                       from tblcustum in tc.DefaultIfEmpty()
                                       where c.IsDelete == false
                                       select new VMTblCustomer  //pake Left join kalo role nya kosong
                                       {
                                           Id = c.Id,
                                           NameCustomer = c.NameCustomer,
                                           Email = c.Email,
                                           Password = c.Password,
                                           Address = c.Address,
                                           Phone = c.Phone,

                                            IdRole = tblcustum.Id,
                                            RoleName = tblcustum.RoleName,

                                           IsDelete = c.IsDelete,
                                           CreateDate = c.CreateDate,
                                       }).ToList();
            return data;
        }


        [HttpGet("CheckByName/{email}/{id}/{IdRole}")]
        public bool CheckName(string email, int id, int idRole)
        {
            TblCustomer data = new TblCustomer();
            if (id == 0)
            {
                data = db.TblCustomers.Where(a => a.Email == email && a.IsDelete == false && a.IdRole == idRole).FirstOrDefault()!;
            }
            else
            {
                data = db.TblCustomers.Where(a => a.Email == email && a.IsDelete == false && a.IdRole == idRole
                && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            return false;
        }



        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }


        [HttpGet("GetDataById/{id}")]
        public VMTblCustomer GetDataById(int id)
        {
            VMTblCustomer data = (from c in db.TblCustomers
                                 join r in db.TblRoles on c.IdRole equals r.Id 
                                  where c.IsDelete == false && c.Id == id
                                 select new VMTblCustomer
                                 {
                                     Id = c.Id,
                                     NameCustomer = c.NameCustomer,
                                     Email = c.Email,
                                     Password = c.Password,
                                     Address = c.Address,
                                     Phone = c.Phone, 
                                     IdRole = c.IdRole,
                                     RoleName = r.RoleName,

                                 }).FirstOrDefault()!;
            return data;
        }



        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Id = data.Id;
                dt.Email = data.Email;
               if(data.Password != null)
                {

                dt.Password = data.Password;
                }
                dt.Address = data.Address;
                dt.Phone = data.Phone;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.message = "Failed saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.message = "Data not found";
            }
            return respon;
        }



        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.message = "Data not found";
            }
            return respon;
        }



    }
}
