﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")] // bisa diganti, kalo kurung keraawal ini manggil nama default nyaa [apiController]
    [ApiController]
    public class apiOrderController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiOrderController(XPOS_334Context _db)
        {
            db = _db;
        }

        [HttpGet("GetAllDataOrderHeader")]
        public List<TblOrderHeader> GetAllDataHeaders()
        {
            List<TblOrderHeader> data = db.TblOrderHeaders.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataOrderDetail/{id}")]
        public List<VMOrderDetail> GetDataDetail(int id)
        {
            List<VMOrderDetail> data = (from d in db.TblOrderDetails
                                        join p in db.TblProducts on d.IdProduct equals p.Id
                                        where d.IsDelete == false && d.IdHeader == id
                                        select new VMOrderDetail
                                        {
                                            Id = d.Id,
                                            Qty = d.Qty,
                                            SumPrice = d.SumPrice,

                                            IdProduct = d.IdProduct,
                                            NameProduct = p.NameProduct,
                                            Price = p.Price,
                                            Stock = p.Stock,

                                        }).ToList();

            return data;
        }

        [HttpPost("SubmitOrder")]
        public VMResponse SubmitOrder(VMOrderHeader dataHeader)
        {
            TblOrderHeader head = new TblOrderHeader();
            head.CodeTransaction = GenerateCode(); //Generate code transaksi (Buat function sendiri)
            head.Amount = dataHeader.Amount;
            head.TotalQty = dataHeader.TotalQty;
            head.IdCustomer = IdUser;
            head.IsCheckOut = true;
            head.IsDelete = false;
            head.CreateBy = IdUser;
            head.CreateDate = DateTime.Now;

            try
            {
                db.Add(head);
                db.SaveChanges();
                foreach (VMOrderDetail item in dataHeader.ListDetails)
                {
                    TblOrderDetail detail = new TblOrderDetail();
                    detail.IdHeader = head.Id;
                    detail.IdProduct = item.IdProduct;
                    detail.Qty = item.Qty;
                    detail.SumPrice = item.SumPrice;
                    detail.IsDelete = false;
                    detail.CreateBy = IdUser;
                    detail.CreateDate = DateTime.Now;

                    try
                    {
                        db.Add(detail);
                        db.SaveChanges();

                        TblProduct dataProduct = db.TblProducts.Find(detail.IdProduct);
                        // TblProduct dataProduct = db.TblProducts.Where(a => a.Id == detail.IdProduct).FirstOrDefault;      pakae salah satu bebeas
                        if (dataProduct != null)
                        {
                            dataProduct.Stock -= detail.Qty;
                            db.Update(dataProduct);
                            db.SaveChanges();
                        }
                        respon.message = "Thanks For Order";

                    }
                    catch (Exception e)
                    {
                        respon.Success = false;
                        respon.message = "Failed To Saved" + e.Message;
                    }

                }
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.message = "Failed To Saved" + e.Message;
            }
            return respon;
        }

        [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = $"XPOS-{DateTime.Now.ToString("yyyyMMdd")}-";
            string digit = "";
            TblOrderHeader data = db.TblOrderHeaders.OrderByDescending(a => a.CodeTransaction).FirstOrDefault();
            if (data != null)
            {
                string codeLast = data.CodeTransaction;
                string[] codeSplit = codeLast.Split('-');
                int intLast = int.Parse(codeSplit[2]) + 1;
                digit = intLast.ToString("00000");
                //   digit = intLast.ToString().PadLeft(5, '0'); sama yang atas mau pake yg mana bebas
            }
            else
            {
                digit = "00001";
            }
            return code + digit;
        }


        [HttpGet("CountTransaction/{IdCostumer}")]
        public int CountTransaction(int IdCostumer)
        {
            int count = 0;
            count = db.TblOrderHeaders.Where(a => a.IsDelete == false && a.IdCustomer == IdCostumer).Count();
            return count;
        }




        [HttpGet("GetDataOrderHeaderDetail/{IdCostumer}")]
        public List<VMOrderHeader> GetDataOrderHeaderDetail(int IdCostumer)
        {
            List<VMOrderHeader> data = (from h in db.TblOrderHeaders
                                        where h.IsDelete == false && h.IdCustomer == IdCostumer
                                        select new VMOrderHeader
                                        {
                                            Id = h.Id,
                                            Amount = h.Amount,
                                            TotalQty = h.TotalQty,
                                            IsCheckOut = h.IsCheckOut,
                                            IdCustomer = h.IdCustomer,
                                            CodeTransaction = h.CodeTransaction,
                                            CreateDate = h.CreateDate,
                                            ListDetails = (from d in db.TblOrderDetails
                                                           join p in db.TblProducts on d.IdProduct equals p.Id
                                                           where d.IsDelete == false && d.IdHeader == h.Id
                                                           select new VMOrderDetail
                                                           {
                                                               Id = d.Id,
                                                               Qty = d.Qty,
                                                               SumPrice = d.SumPrice,
                                                               IdHeader = d.IdHeader,
                                                               IdProduct = d.IdProduct,
                                                               NameProduct = p.NameProduct,
                                                               Price = p.Price,
                                                               Stock = p.Stock


                                                           }).ToList()
                                        }).ToList();
            return data;
        }



    }
}
